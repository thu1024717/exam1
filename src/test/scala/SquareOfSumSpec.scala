import org.scalatest.FunSuite
import Questions._

/** 20%
  * 請用以下函數實現數列加總squareOfSum(n)=1-4+9-16+.....
  * ex:
  * squareOfSum(2)= -3
  * squareOfSum(4)=-10
  * squareOfSum(10)=-55
  * squareOfSum(11)= 66
  **/
class SquareOfSumSpec extends FunSuite {
  test("squareOfSum(1)=1") {
    assert(squareOfSum(1)==1)

  }

  test("squareOfSum(3)=6") {
    assert(squareOfSum(3)==6)

  }
  test("squareOfSum(36)=-666") {
    assert(squareOfSum(36)== -666)

  }
  test("squareOfSum(10)=-55") {
    assert(squareOfSum(10)== -55)

  }
  test("squareOfSum(9)=45") {
    assert(squareOfSum(9)==45)

  }

}
